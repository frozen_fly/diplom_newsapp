export class LocalStorage {
    setProperty(key, data){
        localStorage.setItem(key, JSON.stringify(data))
    }
    getProperty(key){
        return JSON.parse(localStorage.getItem(key))
    }
    getArticleById(id){
        return JSON.parse(localStorage.getItem('current_news'))[id]
    }
}