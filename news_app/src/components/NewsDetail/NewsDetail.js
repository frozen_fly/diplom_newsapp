import React from 'react'
import Title from '../Title/Title'

const NewsDetail = props => {
    if (!props.jsonData) return null;
    const currentNews = props.jsonData.filter((_item, index) => { 
        return index === Number(props.match.params.name)
    })[0]
    const newsTitle = currentNews.title.slice(0, currentNews.title.indexOf(' - '))
    return (
        <section className='news-detail'>
            <div className='news-detail__left-section'>
                <Title text={newsTitle} addClass='title--detail'/>
                <p className='news-detail__source'>{currentNews.source.name}</p>
                <p className='news-detail__date'><span className='news-detail__day'>{currentNews.publishedAt.slice(8,10)}</span> / {currentNews.publishedAt.slice(5,7)}</p>
            </div>
            <div className='news-detail__right-section'>
                <img src={currentNews.urlToImage} alt='Фото новости'/>
                <p className='news-detail__description'>{currentNews.description}</p>
            </div>
        </section>
        )
}

export default NewsDetail;