import React from 'react';
import {Link} from 'react-router-dom';

const Logo = () => {
    return <Link className='logo' to="/main">Новостник</Link>
};

export default Logo;