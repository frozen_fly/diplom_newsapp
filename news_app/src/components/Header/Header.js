import React from 'react';
import {NavLink} from 'react-router-dom';
import Logo from '../Logo/Logo';

const Header = () => {
    return (
        <header className='header'>
            <Logo/>
            <nav className='nav'>
                <ul className='nav__list'>
                    <li className='nav__item'>
                        <NavLink className='nav__link' to="/main">Главная</NavLink>
                    </li>
                    <li className='nav__item'>
                        <NavLink className='nav__link' to="/news">Новости</NavLink>
                    </li>
                    <li className='nav__item'>
                        <NavLink className='nav__link' to="/contacts">Контакты</NavLink>
                    </li>
                </ul>
            </nav>
        </header>
    );
};

export default Header;