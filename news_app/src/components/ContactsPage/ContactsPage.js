import React from 'react'
import contactsPhoto from '../../img/cont_photo.jpg'

const ContactsPage = () => {
    return (
        <section className='contacts'>
            <h2 className='visually-hidden'>Контакты</h2>
            <div className='contacts__wrap'>
                <div className='contacts__text'>
                    <a href='tel:+79163508491' className='contacts__tel'>+7 (916) 350 84 91</a>
                    <div className='contacts__name-wrap'>
                        <p className='contacts__name'>Муха</p>
                        <p className='contacts__name'>Роман</p>
                    </div>
                    <a href='mailto:quarian@mail.ru' className='contacts__email'>quarian@mail.ru</a>
                    <div className='contacts__skills-wrap'>
                        <p className='contacts__proff'>FrontEnd разработчик</p>
                        <p className='contacts__skills'>HTML, CSS, SASS/SCSS, JS, <span className='contacts__last-skill'>React</span></p>
                    </div>
                </div>
                <img className='contacts__img' src={contactsPhoto} alt="Фото Роман Муха"/>
            </div>
        </section>
    )
}

export default ContactsPage;