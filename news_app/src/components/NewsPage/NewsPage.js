import React from 'react'
import NewsItem from '../NewsItem/NewsItem'
import Title from '../Title/Title'

const NewsPage = props => {
    let dataTitle = 'Загрузка...';
    if (props.jsonData !== null) {
        dataTitle = props.jsonData.map((item, index) => {
            return <NewsItem 
                        key={index}
                        itemIndex={index}
                        text={item.title.slice(0, item.title.indexOf(' - '))} //Возвращает элемент новости с текстом из которого вырезается часть до тире
                        source={item.source.name}
                        day={item.publishedAt.slice(8,10)}
                        month={item.publishedAt.slice(5,7)}
                    /> 
        })
    }
    return (
        <section className='news-page'>
            <Title mainText={<span>Быть<br/>в курсе</span>} lastWord='событий'/>
            <div className='news-page__block'>
                {dataTitle}
            </div>
        </section>
    )
}

export default NewsPage;