import React from 'react'
import {Link} from 'react-router-dom';
import {withRouter} from 'react-router-dom';

const NewsItem = props => {
    return (
        <article className='news-item'>
            <Link to={'/news/item_' + props.itemIndex} className='news-item__link'>{props.text}</Link>
            <div className='news-item__bottom'>
                <p className='news-item__source'>{props.source}</p>
                <p className='news-item__date'><span className='news-item__day'>{props.day}</span> / {props.month}</p>
            </div> 
        </article>
    )
}

export default withRouter(NewsItem);

//onClick={() => {props.history.push('/news/item_' + props.itemIndex)}}