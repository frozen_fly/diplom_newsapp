import React from 'react';
import { Link } from 'react-router-dom';
import NewsItem from '../NewsItem/NewsItem';
import Title from '../Title/Title';

const MainPage = props => {
    let dataTitle = 'Загрузка...';
    if (props.jsonData !== null) {
        dataTitle = props.jsonData.map((item, index) => {
            if (index <= 5) {
                return <NewsItem 
                            key={index}
                            itemIndex={index} 
                            text={item.title.slice(0, item.title.indexOf(' - '))} //Возвращает элемент новости с key=индекс в массиве и текстом из которого вырезается часть до тире
                            source={item.source.name}
                            day={item.publishedAt.slice(8,10)}
                            month={item.publishedAt.slice(5,7)}
                        /> 
            } else return null;
        })
    }
    return (
        <section className='main-page'>
            <Title mainText={<span>Всегда<br/> свежие</span>} lastWord='новости'/>
            <div className='main-page__block'>
                {dataTitle}
            </div>
            <Link to="/news" className='main-page__link'>Быть в курсе событий</Link>
        </section>
    )
}

export default MainPage;