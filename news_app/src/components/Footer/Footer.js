import React from 'react';
import Logo from '../Logo/Logo';
import {Link} from 'react-router-dom';

const Footer = () => {
    return (
        <footer className='footer'>
            <div className='footer__left'>
                <Logo/>
                <p className='footer__left-text'>Single Page Application</p>
            </div>
            <p className='footer__text'>Дипломный проект</p>
            <div className='footer__rigth author'>
                <p className='author__text'>Made by</p>
                <Link className='author__link' to='/contacts'>Муха Роман</Link>
            </div>
        </footer>
    );
};

export default Footer;