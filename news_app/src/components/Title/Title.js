import React from 'react'

const Title = props => {
    let modifier = props.addClass;
    if (modifier === undefined) modifier = '';
    let firstPart = null,
        lastPart = null;
    if (props.text === undefined) {
        firstPart = props.mainText
        lastPart = props.lastWord
    } else {
        firstPart = props.text.slice(0, props.text.lastIndexOf(' '));
        lastPart = props.text.slice(props.text.lastIndexOf(' ')+1, props.text.length)
    }
    
    return <h2 className={'title' + ' ' + modifier}>
        {firstPart} <span className='blue-text'>{lastPart}</span>
        </h2>
}

export default Title;