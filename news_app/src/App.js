import React, { useEffect, useState } from 'react';
import axios from 'axios';
import {Route, Redirect, Switch} from 'react-router-dom';

import Header from './components/Header/Header';
import MainPage from './components/MainPage/MainPage';
import NewsPage from './components/NewsPage/NewsPage';
import ContactsPage from './components/ContactsPage/ContactsPage';
import Footer from './components/Footer/Footer';
import NewsDetail from './components/NewsDetail/NewsDetail';

const useFetch = () => {
  const [data, updateStateData] = useState(null);
  const url = 'http://newsapi.org/v2/top-headlines?country=ru&apiKey=6e93507b8b1946128a3d763e0d2e9a6b';
  useEffect(() => {
    axios.get(url).then(response => updateStateData(response.data.articles));
  }, []);
  return data;
}

function App() {

  const result = useFetch();
  if (result !== null) console.log(result);

  return (
    <div className="app main-wrapper">
      <Header />
      <Switch>
        <Route path="/main" exact render={() => <MainPage jsonData={result}/>}/>
        <Route path="/news" exact render={() => <NewsPage jsonData={result}/>}/>
        <Route path="/contacts" exact component={ContactsPage}/>
        <Route path="/news/item_:name" exact render={(props) => <NewsDetail {...props} jsonData={result}/>}/>
        <Redirect from="/" to="/main" />
      </Switch>
      <Footer/>
    </div>
  );
}

export default App;
